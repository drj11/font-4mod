#!/bin/sh

set -e -u

template () {
cat << EOF
P2 8 8 3
0 0 0 0 0 0 0 0
0 a a a a a a 0
0 a b b b b a 0
0 a b c c d a 0
0 a b c c d a 0
0 a d d d d a 0
0 a a a a a a 0
0 0 0 0 0 0 0 0
EOF
}

topng () {
  # Use priweavepng to convert to 8-bit RGB
  pripamtopng - | priweavepng -c 1,1,1 -
}

mkdir -p tile
cd tile

template | tr abcd 1111 | topng > l.png
template | tr abcd 3000 | topng > o.png
template | tr abcd 2030 | topng > r.png
template | tr abcd 1030 | topng > s.png
template | tr abcd 2320 | topng > t.png
template | tr abcd 2202 | topng > z.png

sed 1d ../art/tet-i-h.pgm | (echo P2 8 8 3; cut -d' ' -f 1-8 ) |
  topng > g.png
sed 1d ../art/tet-i-h.pgm | (echo P2 8 8 3; cut -d' ' -f 9-16 ) |
  topng > h.png
sed 1d ../art/tet-i-h.pgm | (echo P2 8 8 3; cut -d' ' -f 25-32 ) |
  topng > i.png

sed 's/32/8/;10,$d' ../art/tet-i-v.pgm | topng > u.png
sed 's/32/8/;2,9d;18,$d' ../art/tet-i-v.pgm | topng > v.png
sed 's/32/8/;2,25d;18,$d' ../art/tet-i-v.pgm | topng > w.png

priforgepng -d 2 -s 8 one | priweavepng -c 1,1,1 - > ,.png
