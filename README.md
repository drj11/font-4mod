# 4mod

A modular font using the Tetris pieces as modules.

## Tiles

8x8 tile images are generated in tile/ by the script

    gen-unit.sh

They are named after the corresponding tetris pieces:

l o r s t z (.png) for the 6 pieces that use a single unit;
g h i (.png) for the 3 parts of the horizontal 4x1.

## Discussion

Starting point: the tetris bricks from the Game Boy Tetris

The reference art art/Tetris-White.png (an essentially random
picture from the internet) happens to have examples of all
pieces.
Comparing it to other pictures of Game Boy Tetris suggests that
it is authentic.

There are 4 layers/colours. On original hardware these 4 shades
of a sort of pale LCD green, the darkest of which is dark enough
to be called black.
By convention the colours shall be:

- white
- light-grey, or light
- dark-grey, or dark
- black

Tetris has 7 pieces. Each piece except the 1x4 I is made from 4
units with the unit being unique to that piece.

However, all of the units can be drawn from a single 5-layer
template, choosing suitable colours for each layer (no unit uses
more than 3 colours). I assume this is some Game Boy hardware
microoptimisation.

The template is

    FFFFFFFF
    FaaaaaaF
    FabbbbaF
    FabccdaF
    FabccdaF
    FaddddaF
    FaaaaaaF
    FFFFFFFF

F (the frame) is always black.

Only one piece, the T, has b != d.

- L (all dark): a=b=c=d = dark
- Z (bicolour): a=b=d = light; c = black
- O (bicolour): a = white; b=c=d = black
- S (triline): b=d black; c = white; a = dark
- R (triline): b=d black; c = white; a = light
- T (shadow): b=white; d=black; a=c = light

The 1x4 piece is constructed with endcaps and two identical
middle units: UVVW.
There is one vertical form and one horizontal form.

# END
